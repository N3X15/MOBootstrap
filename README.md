# MOBootstrap 0.0.1a

Gamers are a lazy sort, and the gamers who come back to heavily-modded games after a long hiatus are often in the dark with regards to to what mods to obtain.
Games like The Elder Scrolls series often need mods in order to run in a stable, satisfactory manner.  
However, bugfix mods change often, frequently needing other bugfix mods, or merge with larger, "umbrella" mods, such as the Unofficial Skyrim patchset.
In the end, users often find themselves downloading dozens of mods just to get going.

This tool is a one-stop shop for installing Mod Organizer (the best such tool on the market, at this time), plus
all the basic mods Skyrim will need to run.  It has its own open-source Python Nexus Mods API, and has an idiot-proof
UI to get the process rolling.  All mods are downloaded directly from the Nexus Mods site to avoid becoming out-of-date,
and to preload Mod Organizer with the data it needs to keep things running smoothly.

# License

MOBootstrap is licensed under the MIT Open Source License.

# Installing

Simply download the latest build and plop it in a directory.  Run it to get going.

## Changing Output Directory

At the moment, MOBootstrap installs Mod Organizer to `C:\Mod Organizer`.  If you wish to put it somewhere else: 

1. Run through the normal steps, but hit CTRL+C after pressing OK on the dialog.
2. Change config.toml to your liking.

# Compiling

This is a highly complex project, utilizing Python, Qt4, YAML, Requests, and TOML, plus
several other packages not available in PIP.  **Follow directions carefully or you will break things.**

1. Clone the repository to the desired directory.
2. Install Python 2.7 from python.org.
3. Run `python prepare.py` and wait for it to finish.

You should now have a working development environment.  To test, run `python bootstrap.py`.

**When committing, remember to remove config.toml!**
