# -*- mode: python -*-
import os
import sys
script_dir = os.path.dirname(os.path.abspath('.'))

sys.path.append(os.path.join(script_dir, 'lib', 'python-build-tools'))
sys.path.append(os.path.join(script_dir, 'lib', 'nexus-api'))
sys.path.append(os.path.join(script_dir, 'lib', 'valve'))

a = Analysis([os.path.join('bootstrap.py')],
    pathex=[
        'lib/python-build-tools',
        'lib/nexus-api',
        'lib/valve'
    ],
    datas=[
        ('icons', 'icons')
    ],
    hiddenimports=['cryptography'],
    hookspath=None)

pyz = PYZ(a.pure)

ename = os.path.join('dist', 'MOBootstrap')
if sys.platform.startswith('win') or sys.platform.startswith('microsoft'):
    ename += '.exe'

exe = EXE(pyz,
    a.scripts,
    a.binaries,
    a.zipfiles,
    a.datas,
    name=ename,
    debug=False,
    strip=None,
    upx=True,
    console=True,
    # icon=os.path.join('icons','mobootstrap.ico')
    )
app = BUNDLE(exe, name='MOBootstrap.app')
