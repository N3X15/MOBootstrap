'''
updatePackages.py - Download and patch .pak files.

Copyright (c) 2015 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''
import toml
import yaml
import os
import sys

script_dir = os.path.dirname(os.path.realpath(__file__))

sys.path.append(os.path.join(script_dir, 'lib', 'python-build-tools'))
sys.path.append(os.path.join(script_dir, 'lib', 'nexus-api'))

from buildtools import os_utils, log, Chdir
from buildtools.config import BaseConfig
from nexusmods import RepoResolver, GAME_SKYRIM, REPO_NEXUSMODS, MOB_VERSION
from nexusmods.ModPackage import ModPackage, ModFile, FileCategory


def nexusDownloadAndExtract(file_id, outpath):
    filepath = nexus.DownloadTo(file_id, DOWNLOADS_PATH)
    os_utils.ensureDirExists(outpath, noisy=True)
    with Chdir(outpath):
        os_utils.decompressFile(filepath)


def updatePackage(pack_id, pack_config):
    with log.info('Updating package "%s"...', pack_id):
        modinfo = ModPackage()
        modinfo.SetUniqueID(pack_id)
        moddata = api.GetModInfo(pack_id)
        repo = api.repositories[modinfo.repo_id]
        modinfofile = os.path.abspath(os.path.join('packages', modinfo.repo_id, repo.game_name, '{}.pak'.format(modinfo.mod_id)))
        packagedir = os.path.dirname(modinfofile)
        os_utils.ensureDirExists(packagedir)
        #print(packagedir)
        modinfo.SetUniqueID(pack_id)
        if os.path.isfile(modinfofile):
            with log.info('Loading %s...', modinfofile):
                modinfo.load('.')
        if moddata:
            modinfo.fromObject(moddata.toObject())
        with log.info('Loading files...'):
            for modfile in api.GetModFiles(modinfo.GetUniqueID(), pack_config):
                # print(modfile)
                mf = ModFile()
                mf.fromObject(modfile.toObject())
                if modinfo.extract_from != '.':
                    mf.extract_from = modinfo.extract_from
                if modinfo.extract_to != 'DATA':
                    mf.extract_to = modinfo.extract_to
                filetype = '???'
                if modfile.category_id == FileCategory.MAIN:
                    modinfo.files[mf.file_id] = mf
                    filetype = 'MAIN'
                elif modfile.category_id == FileCategory.OPTIONAL:
                    modinfo.optional[mf.file_id] = mf
                    filetype = 'OPTIONAL'
                log.info('[%s] %s', filetype, mf.GetUniqueID())
        # print(pack_config)
        modinfo.fromIndex(pack_config)
        modinfo.save('.')
        log.info('Wrote %s.pak with %s file definitions.', modinfo.GetUniqueID(), len(modinfo.files))

with log.info('MOBootstrap - v%s', MOB_VERSION):
    log.info('Copyright (c) 2015-2016 Rob "N3X15" Nelson')
    log.info('Available under the MIT Open-Source License.')

config = {}
with log.info('Reading config.toml...'):
    with open('config.toml', 'r') as f:
        config = toml.loads(f.read())
PACKAGES = {}
with log.info('Reading packages.yaml...'):
    with open('packages/index.yml', 'r') as f:
        PACKAGES = yaml.load(f)

newconfig = BaseConfig()
newconfig.cfg = config
config = newconfig

MO_PATH = config.get('paths.mod-organizer')
DOWNLOADS_PATH = config.get('paths.downloads')

api = None
with log.info('Logging into Skyrim Nexus...'):
    api = RepoResolver(config['auth'], GAME_SKYRIM)
    nexus = api.repositories['nexus']
    if nexus.Login():
        log.info('Username  : %s', nexus.username)
        log.info('Member ID : %s', nexus.member_id)
        log.info('Session ID: %s', nexus.sid)
    else:
        log.critical('Failed to log in.')
        sys.exit(1)

REPOSELECT = {
    'nexus': nexus
}

for bundle, bundledata in PACKAGES.iteritems():
    for pack, packdata in bundledata['packages'].iteritems():
        updatePackage(pack, packdata)
