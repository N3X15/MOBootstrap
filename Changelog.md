# 0.0.3 alpha

## New Features
* New path configuration dialog, for those of us who have expensive eSATA drive enclosures and/or SSDs.
* ENB Changer support added.
* LOOT support added.
* Save Game Script Cleaner added.
* Added support for modifying QSettings-based ModOrganizer configuration files, including ModOrganizer.ini and meta.ini files.
* ENB Changer, SGSC, and LOOT added to Mod Organizer's startup menu.
* Mod Organizer told how to order the mods, given appropriate dependency information in index.yml.

## Bugfixes
* Using custom PyParsing-based VDF parser for figuring out where your libraries are. Solves a crash.
* Fixed "FOMod installer specified copying dir "%s", which doesn't exist."
* MOBootstrap's dependency resolution system has been fixed.

# 0.0.2 alpha

## New Features
* FOMod installer support.  No UI is displayed, plugins to install are pre-defined in the .pak, makes things far less tedious.  Capable of autodetecting DLC plugins.
* Only extracts from archive when needed.
* Detection of which archives need `extract_from`.
* Removal of old files from both cache and MO install
* WIP: Graphical Overhauls bundle

## Bugfixes
* Renamed a bunch of packages to look like less of a clusterfuck when loaded in MO.
* Logging system consolidated, fixed wonky indentation.

# 0.0.1 alpha

* Initial release
