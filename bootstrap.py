'''
MOBootstrap - A system for automatically installing, configuring, and preloading Mod Organizer.

Copyright (c) 2015 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''


def resource_path(relative):
    return os.path.join(
        os.environ.get(
            "_MEIPASS2",
            os.path.abspath(".")
        ),
        relative
    )

import os
import shutil
import zipfile
import sys
import platform
import toml

MOD_ORGANIZER_DOWNLOAD = 'nexus/skyrim/1000174746'
ENB_CHANGER_DOWNLOAD = 'nexus/skyrim/89712'
LOOT_DOWNLOAD = 'https://github.com/loot/loot/releases/download/v0.8.1/LOOT.v0.8.1.7z'
SGSC_DOWNLOAD = 'nexus/skyrim/1000175537'

PLATFORM = platform.system()

script_dir = os.path.dirname(os.path.realpath(__file__))

sys.path.append(os.path.join(script_dir, 'lib', 'nexus-api'))
sys.path.append(os.path.join(script_dir, 'lib', 'valve'))


from buildtools import os_utils, Chdir, log
from buildtools.config import BaseConfig, YAMLConfig
from buildtools.utils import sha256sum

from PyQt4 import QtGui, QtCore
from mobootstrap.ui import dlgPathSelectImpl, dlgPackageSelectImpl
from mobootstrap.modorganizer import MOSettings

from nexusmods import RepoResolver, GAME_SKYRIM, MOB_VERSION
from nexusmods.ModPackage import ModPackage, DependReport
from nexusmods import http

BIN_PATH = os.path.join(script_dir, 'bin')


def decompressFile(archive, cfg={}):
    '''
    Decompresses the file to the current working directory.
    '''
    #print('Trying to decompress ' + archive)
    show_out = cfg.get('verbose', False)
    if show_out:
        log.info('VERBOSE TIME')
    if archive.endswith('.tar.gz') or archive.endswith('.tgz'):
        if PLATFORM == 'Windows':
            archive = os_utils.cygpath(archive)
        os_utils.cmd([os.path.join(BIN_PATH, 'tar.exe'), 'xzf', archive], echo=True, show_output=show_out, critical=True)
        return True
    elif archive.endswith('.tar.bz2') or archive.endswith('.tbz'):
        if PLATFORM == 'Windows':
            archive = os_utils.cygpath(archive)
        os_utils.cmd([os.path.join(BIN_PATH, 'tar.exe'), 'xjf', archive], echo=True, show_output=show_out, critical=True)
        return True
    elif archive.endswith('.tar.xz'):
        if PLATFORM == 'Windows':
            archive = os_utils.cygpath(archive)
        os_utils.cmd([os.path.join(BIN_PATH, 'tar.exe'), 'xJf', archive], echo=True, show_output=show_out, critical=True)
        return True
    elif archive.endswith('.tar.7z'):
        os_utils.cmd(['7za', 'x', '-aoa', archive], echo=True, show_output=show_out, critical=True)
        if PLATFORM == 'Windows':
            archive = os_utils.cygpath(archive)
        os_utils.cmd([os.path.join(BIN_PATH, 'tar.exe'), 'xf', archive[:-3]], echo=True, show_output=show_out, critical=True)
        os.remove(archive[:-3])
        return True
    elif archive.endswith('.7z'):
        if PLATFORM == 'Windows':
            archive = os_utils.cygpath(archive)
        os_utils.cmd([os.path.join(BIN_PATH, '7za.exe'), 'x', '-aoa', archive], echo=True, show_output=show_out, critical=True)
        return True
    elif archive.endswith('.zip'):
        # unzip is unstable on Windows.
        #os_utils.cmd(['unzip', archive[:-4]], echo=True, show_output=False, critical=True)
        with zipfile.ZipFile(archive) as arch:
            arch.extractall('.')
        return True
    elif archive.endswith('.rar'):
        # if PLATFORM == 'Windows':
        #    archive = os_utils.cygpath(archive)
        os_utils.cmd([os.path.join(BIN_PATH, 'unrar.exe'), 'x', '-y', archive, '.'], echo=True, show_output=show_out, critical=True)
        return True
    else:
        log.critical(u'Unknown file extension: %s', archive)
    return False


def installUtility(api, name, dlspec, extract_to, checkfile, extract_from='.'):
    if not os.path.isfile(os.path.join(extract_to, checkfile)):
        with log.info('Installing %s...', name):
            tmpdir = os.path.join(DOWNLOADS_PATH, name)
            nexusDownloadAndExtract(api, dlspec, tmpdir, clean=False)
            with Chdir(os.path.join(tmpdir, extract_from)):
                log.info('Copying to %s...', extract_to)
                os_utils.copytree('.', extract_to)
    else:
        log.info('%s is already installed, skipping installation.', name)


def nexusDownloadAndExtract(api, file_id, outpath, check_lock=False, clean=True, dl_config={}, x_config={}):
    if file_id.startswith('http'):
        filepath = http.FetchDownload(file_id, DOWNLOADS_PATH, **dl_config)
    else:
        filepath = api.DownloadTo(file_id, DOWNLOADS_PATH, dl_config)
    os_utils.ensureDirExists(outpath, noisy=True)

    extract = True
    hashsum = ''
    extract_lock = filepath + '.mob-xlock'
    if check_lock:
        hashsum = sha256sum(filepath)
        storedsum = ''
        if os.path.isfile(extract_lock) and os.path.isdir(outpath) and len(os.listdir(outpath)) > 0:
            with open(extract_lock, 'r') as f:
                storedsum = f.read().strip()
                if hashsum == storedsum:
                    extract = False
                    log.info("File hash hasn't changed, skipping re-extraction.")
                else:
                    with log.info('HASH MISMATCH:'):
                        log.info('Stored: {}'.format(storedsum))
                        log.info('Fresh : {}'.format(hashsum))
    if extract:
        if clean:
            log.info('Cleaning old files...')
            os_utils.safe_rmtree(outpath)
        os_utils.ensureDirExists(outpath)
        with Chdir(outpath):
            decompressFile(filepath, cfg=x_config)
    if check_lock:
        with open(extract_lock, 'w') as f:
            f.write(hashsum)
    return filepath

DOWNLOADS_PATH = os.path.join(script_dir, 'cache', 'downloads')
LOCK_PATH = os.path.join(script_dir, 'cache', 'locks')


def kickoff():
    api = None
    nexus = None
    MO_PATH = ''

    files_to_install = []
    mods_and_files_installed = []

    with open(os.path.join(script_dir, 'config.toml'), 'w') as f:
        f.write(toml.dumps(config.cfg))

    MO_PATH = config.get('paths.mod-organizer')
    with log.info('Logging into Skyrim Nexus...'):
        api = RepoResolver(config['auth'], GAME_SKYRIM)
        nexus = api.repositories['nexus']
        if nexus.Login():
            log.info('Username  : %s', nexus.username)
            log.info('Member ID : %s', nexus.member_id)
            log.info('Session ID: %s', nexus.sid)
        else:
            log.critical('Failed to log in.')
            sys.exit(1)

    os_utils.ensureDirExists(DOWNLOADS_PATH, noisy=True)

    GAME_PATH = config.get('paths.skyrim.install')
    ENBC_PATH = config.get('paths.enbchanger')
    SGSC_PATH = config.get('paths.sgsc')
    LOOT_PATH = config.get('paths.loot')

    with log.info('Path settings:'):
        log.info('Skyrim Install: %s', GAME_PATH)
        log.info('ENB Changer...: %s', ENBC_PATH)
        log.info('SaveTool......: %s', SGSC_PATH)
        log.info('LOOT..........: %s', LOOT_PATH)

    installUtility(api, "Mod Organizer", MOD_ORGANIZER_DOWNLOAD, MO_PATH, 'modorganizer.exe', extract_from='ModOrganizer')

    MOCONFIG_PATH = os.path.join(MO_PATH, 'ModOrganizer.ini')
    mo_settings = MOSettings(MOCONFIG_PATH, {})
    with log.info('Saving changes...'):
        mo_settings.Save(os.path.join(MO_PATH, 'ModOrganizer.ini.test_a'))
    # For testing deserialization
    #import yaml
    # with open('MOCONFIG.yml', 'w') as f:
    #    yaml.dump(mo_settings.cfg, f, default_flow_style=False)
    # sys.exit(1)

    installUtility(api, "ENB Changer", ENB_CHANGER_DOWNLOAD, ENBC_PATH, 'enbmanager.jar')
    installUtility(api, "Save Game Script Cleaner", SGSC_DOWNLOAD, SGSC_PATH, 'SaveTool.exe')
    installUtility(api, "LOOT", LOOT_DOWNLOAD, LOOT_PATH, 'LOOT.exe')

    all_packages = {}
    selected_packages = []
    with log.info('Loading mod package data...'):
        selected_pack_ids = []
        for bundle_id, bundle_data in BUNDLES.cfg.iteritems():
            if bundle_id in config['bundles']:
                for pack_id, pack_config in bundle_data['packages'].iteritems():
                    packagedata = ModPackage()
                    packagedata.SetUniqueID(pack_id)
                    packagedata.load('.')
                    if packagedata.canInstall(config):
                        # packagedata.fromIndex(pack_config)
                        selected_packages.append(packagedata)
                    else:
                        log.info('Skipping package "%s" (ineligable after dependency check)', packagedata.Name)

    with log.info('Checking mod selection validity...'):
        nchanges = 0
        while nchanges > 0:
            nchanges = 0
            for mod in list(selected_packages):
                for requirement in mod.required:
                    if requirement not in config['packages']:
                        packagedata = ModPackage()
                        packagedata.SetUniqueID(pack_id)
                        packagedata.load('.')
                        selected_packages.append(packagedata)
                        nchanges += 1
                        log.info('Added missing requirement %s (of %s) to queue.', packagedata.id, mod.id)

    selected_modfiles = []
    with log.info('Pre-sorting packages...'):
        selected_packages.sort(reverse=True)
        for package in selected_packages:
            for f in package.files.values():
                if f.canInstall(config) == DependReport.MET:
                    selected_modfiles.append(f)

    install_sequence = []
    mod_load_order = []
    with log.info('Sorting dependencies...'):
        passnum = 0
        this_pass = list(selected_modfiles)
        while len(this_pass) > 0:
            passnum += 1
            next_pass = []
            def isLastChance():
                return len(next_pass)+1 >= len(this_pass)
            for modfile in this_pass:
                report = modfile.dependenciesMet(config, mods_and_files_installed, isLastChance())
                if report == DependReport.SKIP:
                    log.info('[%d] SKIP %s', passnum, modfile.name)
                    continue
                elif report == DependReport.MET:
                    package = modfile.mod
                    install_sequence.append((package, modfile))
                    if 'package:' + package.id not in mods_and_files_installed:
                        mods_and_files_installed.append('package:' + package.id)
                    if 'modfile:' + modfile.id not in mods_and_files_installed:
                        mods_and_files_installed.append('modfile:' + modfile.id)
                    mod_load_order.append(modfile)
                    if len(modfile.requires+modfile.conflicts) > 0:
                        log.info('[%d] %s (%s)', passnum, modfile.name, ', '.join(modfile.requires))
                    else:
                        log.info('[%d] %s', passnum, modfile.name)
                else:  # WAIT
                    next_pass.append(modfile)
                    log.debug('[%d] WAIT - %s', passnum, modfile.name)
            log.info([n.name for n in next_pass])
            if len(next_pass) < len(this_pass):
                log.debug('[%d] CHK - %d vs %d', passnum, len(next_pass), len(this_pass))
            else:
                with log.critical('[%d] CHK - %d vs %d', passnum, len(next_pass), len(this_pass)):
                    for modfile in next_pass:
                        with log.info('[%d] %s', passnum, modfile.name):
                            if len(modfile.requires)>0:
                                with log.info('requires:'):
                                    for req in modfile.requires:
                                        log.info('+ '+req)
                            if len(modfile.conflicts)>0:
                                with log.debug('conflicts:'):
                                    for req in modfile.conflicts:
                                        log.info('- '+req)
                log.info('Please make a bug report with the above output.  A dependency statement either does not exist, or was circular.')
                sys.exit(1)
            this_pass = list(next_pass)

    with log.info('Installing %d mods...', len(install_sequence)):
        for mod, modfile in install_sequence:
            with log.info('Installing %s (%s)...', modfile.id, modfile.GetUniqueID()):
                tmpdir = os.path.join(DOWNLOADS_PATH, modfile.id)
                destdir = ''
                if modfile.extract_to == 'ENB_VERSION_DIR':
                    destdir = os.path.join(ENBC_PATH, 'ENB Versions', modfile.name)
                elif modfile.extract_to == 'ENB_CONFIG_DIR':
                    destdir = os.path.join(ENBC_PATH, 'ENB Configs', modfile.name)
                elif modfile.extract_to == 'DATA':
                    destdir = os.path.join(MO_PATH, 'mods', modfile.name)
                if os.path.isdir(destdir) and len(os.listdir(destdir)) > 0 and modfile.CheckLock(LOCK_PATH):
                    log.info('Already installed, skipping.')
                else:
                    dlfile = nexusDownloadAndExtract(api, modfile.GetUniqueID(for_dl=True), tmpdir, check_lock=True, dl_config=modfile.download_config, x_config=modfile.extract_config)

                    if os.path.isdir(os.path.join(tmpdir, 'Data')) and modfile.extract_from != 'Data':
                        new_extractfrom = os.path.join(modfile.extract_from, 'Data')
                        log.warn('[%s] Data/ found in tmpdir, recommend using `extract_from: "%s"` in package.yml!', modfile.id, new_extractfrom)

                    if os.path.isdir(destdir):
                        log.info('Cleaning directory of old files...')
                        os_utils.safe_rmtree(destdir)
                    os_utils.ensureDirExists(destdir)
                    if modfile.installer is None:
                        log.info('Copying to %s...', destdir)
                        os_utils.copytree(os.path.abspath(os.path.join(tmpdir, modfile.extract_from)), destdir)
                        modfile.writeMetaINI(mod, os.path.join(destdir, 'meta.ini'), os.path.basename(dlfile))
                    else:
                        with Chdir(os.path.abspath(os.path.join(tmpdir, modfile.extract_from))):
                            modfile.installer.install(destdir, config, mods_and_files_installed)

                    log.info('Tricking Mod Organizer into thinking IT downloaded it...')
                    dlpath = os.path.join(MO_PATH, 'downloads')
                    os_utils.ensureDirExists(dlpath)
                    shutil.copy2(dlfile, dlpath)
                    modfile.writeDLMeta(mod, os.path.join(dlpath, os.path.basename(dlfile) + '.meta'), dlfile)

                    modfile.SetLock(LOCK_PATH)

    with log.info('Configuring Mod Organizer...'):
        with log.info('Checking default applications...'):
            apps = mo_settings.applications()
            default_apps = ['SKSE', 'Skyrim', 'Skyrim Launcher', 'Creation Kit']
            for defapp in default_apps:
                if defapp not in apps:
                    mo_settings.addApplication(title=defapp)
            mo_settings.setApplication('SKSE',
                                       title='SKSE',
                                       binary=os.path.join(GAME_PATH, 'skse_loader.exe'),
                                       custom=True,
                                       closeOnStart=False)
            mo_settings.setApplication('Creation Kit',
                                       title='Creation Kit',
                                       binary=os.path.join(GAME_PATH, 'CreationKit.exe'),
                                       custom=True,
                                       closeOnStart=False,
                                       steamAppID=202480)
        with log.info('Adding new applications...'):
            # if 'ENB Changer' not in apps:
            mo_settings.setApplication('ENB Changer',
                                       title='ENB Changer',
                                       binary=os_utils.which('javaw'),
                                       arguments=os.path.join(ENBC_PATH, 'enbmanager.jar'),
                                       workingDirectory=ENBC_PATH,
                                       noisy=True)
            # if 'Save Game Script Cleaner' not in apps:
            mo_settings.setApplication('Save Game Script Cleaner',
                                       title='Save Game Script Cleaner',
                                       binary=os.path.join(SGSC_PATH, 'SaveTool.exe'),
                                       noisy=True)
            # if 'LOOT' not in apps:
            mo_settings.setApplication('LOOT',
                                       title='LOOT',
                                       binary=os.path.join(LOOT_PATH, 'LOOT.exe'),
                                       noisy=True)
        with log.info('Saving changes...'):
            mo_settings.Save(os.path.join(MO_PATH, 'ModOrganizer.ini'))
        with log.info('Creating Default profile...'):
            profile_dir = os.path.join(MO_PATH,'profiles','Default')
            os_utils.ensureDirExists(profile_dir,noisy=True)
            with log.info('Saving modlist.txt...'):
                with open(os.path.join(profile_dir,'modlist.txt'),'w') as modlist:
                    modlist.write('# This file was automatically generated by MOBootstrap v{}.\n'.format(MOB_VERSION))
                    for modfile in reversed(mod_load_order):
                        modlist.write('+ {}\n'.format(modfile.name))
                    # MO will add unmanaged shit.
    log.info('DONE!')
    log.info('Remember to tell MO to sort your load order with LOOT.')

with log.info('MOBootstrap v%s - https://gitlab.com/N3X15/MOBootstrap', MOB_VERSION):
    log.info('Copyright (c) 2015-2016 Rob "N3X15" Nelson')
    log.info('Available under the MIT Open-Source License.')

config = {
    'paths': {},
    'auth': {
        'nexus': {
            'username': '',
            'password': '',
        }
    },
    'bundles': []
}

BUNDLES = YAMLConfig('packages/index.yml', {})
if os.path.isfile('config.toml'):
    with log.info('Reading config.toml...'):
        with open('config.toml', 'r') as f:
            config = toml.loads(f.read())

newconfig = BaseConfig()
newconfig.cfg = config
config = newconfig

app = QtGui.QApplication(sys.argv)

pathSelector = dlgPathSelectImpl(config, BUNDLES)
if not pathSelector.exec_():
    sys.exit(0)  # Cancelled.

packageSelector = dlgPackageSelectImpl(config, BUNDLES, kickoff)
if packageSelector.exec_():
    log.info('User hit OK, we have kickoff().')
    kickoff()
