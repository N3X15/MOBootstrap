'''
Manual "Installer".

Copyright (c) 2015 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''
import shutil
import os
from buildtools import os_utils
from buildtools.bt_logging import log


class Manual(object):

    '''
    Interface for handling manual mod installation.
    '''

    def __init__(self):
        self.name = ''
        self.config = {}

        self.picked_files = {}
        #: A list of strings.
        self.excluded_files = []

    def reset(self):
        self.__init__()

    def Configure(self, cfg):
        self.config = cfg
        self.picked_files = cfg['picked']
        self.excluded_files = cfg.get('excluded', [])

    def getConfig(self):
        return {
            'type': 'manual',
            'picked': self.picked_files,
            'excluded': self.excluded_files
        }

    def install(self, to_path, global_cfg, installed):
        with log.info('Installing...'):
            for root, dirs, files in os.walk('.'):
                for filename in files + dirs:
                    relpath = os.path.relpath(os.path.join(root, filename))
                    stdpath = relpath.replace('\\', '/')
                    #print(stdpath)
                    if stdpath in self.excluded_files:
                        log.info('Skipping %s', stdpath)
                        continue
                    elif stdpath in self.picked_files:
                        newpath = os.path.abspath(os.path.join(to_path, self.picked_files[stdpath]))
                        log.info('Copying %s -> %s', relpath, newpath)
                        if os.path.isfile(relpath):
                            shutil.copy2(relpath, newpath)
                        elif os.path.isdir(relpath):
                            os_utils.copytree(relpath, newpath)
