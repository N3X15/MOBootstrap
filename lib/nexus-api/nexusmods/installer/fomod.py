'''
Simple, stupid FOMod parser for nexusmods API.

Copyright (c) 2015 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''
import os
import shutil

from lxml import etree

from buildtools import log, os_utils
from buildtools.utils import getClassName
#from buildtools import lxml_utils


def tagToUnicode(tag):
    return unicode(tag.text)


def attrToUnicode(attr):
    def convert(tag):
        return unicode(tag.attrib[attr])
    return convert


def represent(representation):
    def convert(tag):
        #print('Representing {} as {}'.format(tag.tag, representation))
        representative = representation()
        representative.fromXML(tag)
        return representative
    return convert


class LXmlNodeRepresentation(object):

    def __init__(self, tagname):
        self.__tagName = tagname
        self.__attribute_mappings = {}
        self.__childnode_mappings = {}

    def assocAttr(self, attrName, fldName='', converter=unicode):
        if fldName == '':
            fldName = attrName
        self.__attribute_mappings[attrName] = (fldName, converter)

    def assocNode(self, tagName, fldName='', converter=None, is_list=False):
        if converter is None:
            converter = represent(LXmlNodeRepresentation)
        if fldName == '':
            fldName = tagName
        self.__childnode_mappings[tagName] = (fldName, converter, is_list)

    def fromXML(self, node):
        for attrName in node.attrib:
            if attrName not in self.__attribute_mappings:
                log.warn('Unknown attribute in %s: "%s"', self.__tagName, attrName)
                continue
            fldName, convMethod = self.__attribute_mappings[attrName]
            if fldName is None:
                continue
            setattr(self, fldName, convMethod(node.attrib[attrName]))
        for tag in node:
            #print('{} CHILD NODE: <{}> '.format(self.__tagName, tag))
            tagName = tag.tag
            if tagName not in self.__childnode_mappings:
                log.warn('Unknown tag in %s: "%s"', self.__tagName, tagName)
                continue
            fldName, convMethod, is_list = self.__childnode_mappings[tagName]
            if fldName is None:
                continue
            if is_list:
                setattr(self, fldName, [])
                for listElement in tag:
                    data = convMethod(listElement)
                    if data:
                        getattr(self, fldName).append(data)
            else:
                setattr(self, fldName, convMethod(tag))


class FOMod(LXmlNodeRepresentation):

    '''
    Interface for handling FOMODs.
    '''

    def __init__(self):
        super(FOMod, self).__init__('config')
        self.installSteps = []
        self.name = ''
        self.required = []

        self.config = {}

        self.assocAttr('name')
        self.assocNode('moduleName', None)  # Not used
        self.assocNode('requiredInstallFiles', 'required', is_list=True)
        self.assocNode('installSteps', converter=represent(FOModInstallStep), is_list=True)

    def reset(self):
        self.__init__()

    def Load(self, filename):
        with open(filename, 'r') as f:
            self.fromXML(etree.parse(f).getroot())

    def Configure(self, cfg):
        self.config = cfg

    def canInstallSubpackage(self, global_cfg, installed, subpkg, subpkg_cfg):
        if subpkg_cfg == True:
            return True

        if 'requires' in subpkg_cfg:
            for required in subpkg_cfg['requires']:
                if not self.checkRequirement(subpkg_cfg, required, installed, global_cfg):
                    return False

        if 'conflicts' in subpkg_cfg:
            for conflict in subpkg_cfg['conflicts']:
                if self.checkRequirement(subpkg_cfg, conflict, installed, global_cfg):
                    return False
        return True

    def checkRequirement(self, cfg, required, installed, global_cfg):
        if required.startswith(':'):
            directive, param = required.split(':')
            if directive == 'gamefile':
                install_dir = global_cfg.cfg['paths'][self.game_id]['install']
                if not os.path.isfile(os.path.join(install_dir, param)):
                    return False
            elif directive == 'package':
                if installed is not None and param not in installed:
                    return False
        return True

    def getConfig(self):
        return {'type': 'fomod', 'selections': self.config.get('selections', [])}

    def getAvailableOptions(self):
        opts = []
        for step in self.installSteps:
            path = [step.name]
            step_out = []
            for optGroup in step.optionalGroups:
                group = {}
                group_path = path + [optGroup.name]
                for plugin in optGroup.plugins:
                    plugpath = group_path + [plugin.name]
                    group[str('/' + '/'.join(plugpath))] = False
                step_out += [{'type': optGroup.type, 'options': group}]
            opts.append(step_out)
        return opts

    def setOptions(self, options):
        for step in self.installSteps:
            cpath = [step.name]
            for optGroup in step.optionalGroups:
                group_path = cpath + [optGroup.name]
                for plugin in optGroup.plugins:
                    plugpath = group_path + [plugin.name]
                    path = str('/' + '/'.join(plugpath))
                    if path in options:
                        log.info('Enabled %s', path)
                        plugin.enabled = True
                    else:
                        plugin.enabled = False

    def install(self, to_path, global_cfg, installed):
        # Look like:
        # /step/group/plugin
        # If they're present, they're enabled.
        with log.info('Configuring FOMod Installer...'):
            self.Load('FOMod/ModuleConfig.xml')
            selected_opts = []
            # print(repr(self.config))
            for subpkg, subpkg_cfg in self.config.get('selections', {}).iteritems():
                if self.canInstallSubpackage(global_cfg, installed, subpkg, subpkg_cfg):
                    selected_opts += [subpkg]
            self.setOptions(selected_opts)
        with log.info('Installing...'):
            import yaml
            with open('fomod_opts.yml', 'w') as f:
                yaml.dump(self.getAvailableOptions(), f, default_flow_style=False)
            for step in self.installSteps:
                step.install(to_path)


class FOModInstallStep(LXmlNodeRepresentation):

    def __init__(self):
        super(FOModInstallStep, self).__init__('installStep')
        self.name = ''
        self.optionalGroups = []

        self.assocAttr('name')
        self.assocNode('optionalFileGroups', 'optionalGroups', converter=represent(FOModPluginGroup), is_list=True)

    def install(self, to_path):
        with log.info('Step %s:', self.name):
            for group in self.optionalGroups:
                group.install(to_path)


class FOModPluginGroup(LXmlNodeRepresentation):
    SelectExactlyOne = 'SelectExactlyOne'
    SelectExactlyOne = 'SelectAtMostOne'

    def __init__(self):
        super(FOModPluginGroup, self).__init__('group')
        self.name = ''
        self.type = self.SelectExactlyOne
        self.plugins = []

        self.assocAttr('name')
        self.assocAttr('type')
        self.assocNode('plugins', converter=represent(FOModPlugin), is_list=True)

    def install(self, to_path):
        with log.info('Group %s: (%d plugins)', self.name, len(self.plugins)):
            for plugin in self.plugins:
                plugin.install(to_path)


class FOModPlugin(LXmlNodeRepresentation):

    def __init__(self):
        super(FOModPlugin, self).__init__('plugin')
        self.name = ''
        self.description = ''
        self.image = ''
        self.conditionsFlags = []  # I don't even
        self.typeDescriptor = []  # I don't even

        self.files = []

        self.assocAttr('name')

        self.assocNode('conditionFlags', converter=attrToUnicode('name'), is_list=True)
        self.assocNode('typeDescriptor', converter=attrToUnicode('name'), is_list=True)

        self.assocNode('description', converter=tagToUnicode)
        self.assocNode('image', converter=attrToUnicode('path'))
        self.assocNode('files', converter=self.fileOrFolder, is_list=True)

        self.enabled = False

    def install(self, to_path):
        if not self.enabled:
            return False
        with log.info('Installing Plugin "%s"...', self.name):
            for installElem in self.files:
                installElem.install(to_path)

    def fileOrFolder(self, tag):
        rep = None
        if tag.tag == 'folder':
            rep = FOModFolder()
        elif tag.tag == 'file':
            rep = FOModFile()
        else:
            log.warn('Unknown tag "%s" in %s.fileOrFolder', tag.tag, self.__tagName)
            return None
        rep.fromXML(tag)
        return rep


class FOModFolder(LXmlNodeRepresentation):

    def __init__(self):
        super(FOModFolder, self).__init__('folder')
        self.source = ''
        self.destination = ''

        self.assocAttr('source')
        self.assocAttr('destination')

    def install(self, to_path):
        if not os.path.isdir(self.source):
            log.warn('FOMod installer specified copying from dir "%s", which doesn\'t exist.',self.source)
        else:
            os_utils.copytree(self.source, os.path.join(to_path, self.destination))


class FOModFile(LXmlNodeRepresentation):

    def __init__(self):
        super(FOModFile, self).__init__('file')
        self.source = ''
        self.destination = ''

        self.assocAttr('source')
        self.assocAttr('destination')

    def install(self, to_path):
        if not os.path.isfile(self.source):
            log.warn('FOMod installer specified copying file "%s", which doesn\'t exist.', self.source)
        else:
            truedest = os.path.join(to_path, self.destination)
            os_utils.ensureDirExists(os.path.dirname(truedest))
            shutil.copy2(self.source, truedest)
