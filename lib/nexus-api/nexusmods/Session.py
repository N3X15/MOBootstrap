'''
API Magick

Copyright (c) 2015 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''
import os
import sys
import requests
import json

from ._globals import *
from ModPackage import ModPackage, ModFile, FileCategory
import http
from buildtools.bt_logging import log, NullIndenter

REPO_NEXUSMODS = 'nexus'
REPO_NEXISONLINE = 'n3x15'

Repositories = {
    'nexus': 'nmm.nexusmods.com',
    'n3x15': 'nmm.nexisonline.net',
}


class HTTPResponse(object):

    def __init__(self, res):
        self.code = -1
        self.headers = {}
        self.content = ''
        self.cookies = {}
        if res is not None:
            self.code = res.status_code
            self.headers = dict(res.headers)
            self.cookies = dict(res.cookies)
            self.content = res.content

    @property
    def valid(self):
        return self.code != -1

    def toObject(self):
        return json.loads(self.content)

GAME_SKYRIM = 110

Games = {
    110: 'skyrim'
}

import logging

logging.getLogger("requests").setLevel(logging.WARNING)


class APISession(object):

    def __init__(self, repo_id, game_id, username, password):
        self.repo_id = repo_id
        self.game_id = game_id
        self.game_name = Games[game_id]
        self.member_id = 0
        self.sid = None
        self.username = username
        self.password = password
        self.base_uri = 'http://{NMM_SERVER}/{GAME_NAME}'.format(NMM_SERVER=Repositories[repo_id], GAME_NAME=self.game_name)

        self.cookies = {}

    def POST(self, uri_fragment, data={}, suppress_errors=(), suppress_read_msg=True, allow_redirects=False):
        return self.tryFetchURL(uri_fragment, method=http.METHOD_POST, data=data, suppress_errors=suppress_errors, suppress_read_msg=suppress_read_msg, allow_redirects=allow_redirects)

    def GET(self, uri_fragment, data={}, suppress_errors=(), suppress_read_msg=True, allow_redirects=False):
        return self.tryFetchURL(uri_fragment, method=http.METHOD_GET, data=data, suppress_errors=suppress_errors, suppress_read_msg=suppress_read_msg, allow_redirects=allow_redirects)

    def PUT(self, uri_fragment, data={}, suppress_errors=(), suppress_read_msg=True, allow_redirects=False):
        return self.tryFetchURL(uri_fragment, method=http.METHOD_PUT, data=data, suppress_errors=suppress_errors, suppress_read_msg=suppress_read_msg, allow_redirects=allow_redirects)

    def tryFetchURL(self, uri_fragment, data={}, referer='', suppress_errors=(), suppress_read_msg=True, method=http.METHOD_GET, allow_redirects=False):
        uri = self.base_uri + uri_fragment
        lh = NullIndenter()
        if not suppress_read_msg:
            lh = log.info('[{0}] Attempting to read {1}...'.format(self.repo_id, uri))
        with lh:
            try:
                res = http.sendRequest(method, uri, cookies=self.cookies, headers={'Referer': referer}, data=data, allow_redirects=allow_redirects)
            except requests.exceptions.ConnectionError as e:
                log.warn('[{0}] Connection Error: {1}'.format(self.repo_id, uri))
                log.warn(e)
                return HTTPResponse(None)
            except requests.exceptions.RequestException:
                log.warn('[{0}] Request Exception: {1}'.format(self.repo_id, uri))
                return HTTPResponse(None)

            if(res.status_code != 200):
                if res.status_code not in suppress_errors:
                    log.warn("[{0}] Error code: {1} - {2}".format(self.repo_id, res.status_code, uri))
                # log.info(res.text)
                return HTTPResponse(res)

            if res.headers is None:
                log.warn('[{0}] Connection error (no headers): {1}'.format(self.repo_id, uri))
                return HTTPResponse(None)

            return HTTPResponse(res)

    def Login(self):
        return False


class NexusModsSession(APISession):

    def __init__(self, repo_id, game_id, username, password):
        super(NexusModsSession, self).__init__(repo_id, game_id, username, password)

        self.categories = {
            '1': FileCategory.MAIN,
            '3': FileCategory.OPTIONAL
        }

    def Login(self):
        if self.sid is not None:
            return
        res = self.POST('/Sessions/?Login&uri=http%3A%2F%2Fnmm.nexusmods.com%2F' + self.game_name, data={'username': self.username, 'password': self.password}, suppress_errors=(302,))
        if not res.valid:
            log.info('Invalid response.')
            return False
        if res.code != 302:
            log.info('Error code: %d', res.code)
            return False
        if 'sid' not in res.cookies:
            return False
        self.sid = res.cookies['sid']
        self.member_id = res.cookies['member_id']

        # log.info('Logged in as member #%d, SID %s', self.member_id, self.sid)
        return True

    def ResolveFileKey(self,key):
        return key

    def GetFileDownload(self, file_id):
        self.Login()
        file_id = int(file_id)
        with log.info('Enumerating available downloads for file #%d...', file_id):
            fileinfo = self.GET('/Files/download/{file_id}?game_id={game_id}'.format(file_id=file_id, game_id=self.game_id))

            '''
            [
                {
                    "URI":"http:\\/\\/filedelivery.nexusmods.com\\/110\\/1000147100\\/Jaxonz%20Diagnostics-62346-1-1.zip?ttl=1450159645&ri=8192&rs=8192&setec=732649672819adda62ce4624ef84e262",
                    "IsPremium":false,
                    "Name":"CDN",
                    "Country":"",
                    "ConnectedUsers":0
                }
            ]
            '''

            least_premium_data = None
            least_premium_users = 99999
            least_data = None
            least_users = 99999

            for available_file in fileinfo.toObject():
                if available_file['IsPremium']:
                    if least_premium_users > available_file['ConnectedUsers']:
                        least_premium_data = available_file
                        least_premium_users = available_file['ConnectedUsers']
                else:
                    if least_users > available_file['ConnectedUsers']:
                        least_data = available_file
                        least_users = available_file['ConnectedUsers']

            best_data = {}
            if least_premium_data is not None:
                best_data = least_premium_data
            else:
                best_data = least_data
            log.info('Best available server: {name} - {num_users} users, {premium}'.format(name=best_data['Name'], num_users=best_data['ConnectedUsers'], premium='premium' if best_data['IsPremium'] else 'non-premium'))
            return best_data['URI']

    def GetModInfo(self, mod_id):
        self.Login()
        # GET /skyrim/Mods/62346/?game_id=110 HTTP/1.1
        with log.info('Getting information for mod #%d...', mod_id):
            res = self.GET('/Mods/{mod_id}/?game_id={game_id}'.format(mod_id=mod_id, game_id=self.game_id))
            modinfo = self.objectToPackage(res.toObject())
            return modinfo

    def objectToPackage(self, json):
        '''
        :param json dict:
        '''
        import HTMLParser
        h = HTMLParser.HTMLParser()
        json['name'] = h.unescape(json['name'])
        package = ModPackage()
        if package.id == '':
            package.id = json['name'].lower()
        if package.mod_id == 0:
            package.mod_id = json['id']
        if package.name == '':
            package.name = json['name']
        package.repo_id = self.repo_id
        package.game_id = self.game_name
        package.author = json['author']
        package.adult = json['adult'] == 1
        package.categories = [int(x) for x in str(json['category_id']).split(',')]
        package.version = json['version']
        return package

    def objectToFile(self, json):
        '''
        :param json dict:
        '''
        import HTMLParser
        h = HTMLParser.HTMLParser()
        json['name'] = h.unescape(json['name'])
        fileinfo = ModFile()
        fileinfo.mod_id = int(json['mod_id'])
        fileinfo.repo_id = self.repo_id
        fileinfo.game_id = self.game_name
        fileinfo.name = json['name']
        fileinfo.file_id = json['id']
        fileinfo.id = json['name'].lower()
        fileinfo.version = json['version']
        fileinfo.category_id = self.categories.get(str(json['category_id']), -1)
        return fileinfo

    def GetModFiles(self, mod_id, pack_config=None):
        self.Login()
        with log.info('[%s] Getting file list for mod #%d...', self.repo_id, mod_id):
            res = self.GET('/Files/indexfrommod/{mod_id}?game_id={game_id}'.format(mod_id=mod_id, game_id=self.game_id))
            files = []
            for fileinfo in res.toObject():
                files.append(self.objectToFile(fileinfo))
            log.info('Found %d files', len(files))
            return files

    def DownloadTo(self, file_id, to_path, dl_cfg):
        file_uri = self.GetFileDownload(file_id)
        return http.FetchDownload(file_uri, to_path, **dl_cfg)


class DirectSession(object):

    def __init__(self, repo_id, game_id):
        self.repo_id = repo_id
        self.game_id = game_id
        self.game_name = Games[game_id]

        self.file_id = 0  # For assigning unique file IDs.
        self.file_uri2id = {}

    def Login(self):
        return True

    def GetFileDownload(self, file_id):
        return file_id  # honk

    def ResolveFileKey(self, key):
        return self.file_uri2id.get(key, key)

    def GetModInfo(self, mod_id):
        mod = ModPackage()
        mod.repo_id = self.repo_id
        mod.game_id = self.game_name
        mod.mod_id = int(mod_id)
        return mod

    def GetModFiles(self, mod_id, pack_config=None):
        o = []
        # print(repr(pack_config))
        if pack_config:
            cats = [
                (FileCategory.MAIN, pack_config.get('files', {})),
                (FileCategory.OPTIONAL, pack_config.get('optional', {})),
            ]
            for cat_id, data in cats:
                for file_key, file_info in data.iteritems():
                    modfile = ModFile()
                    modfile.repo_id = self.repo_id
                    modfile.game_id = self.game_name
                    modfile.mod_id = mod_id
                    self.file_id += 1
                    modfile.file_id = self.file_id
                    modfile.url = file_key
                    modfile.category_id = cat_id
                    modfile.fromIndex(file_info)
                    self.file_uri2id[file_key] = modfile.file_id
                    o.append(modfile)
        return o

    def DownloadTo(self, file_id, to_path, dl_cfg):
        file_uri = self.GetFileDownload(file_id)
        return http.FetchDownload(file_uri, to_path, **dl_cfg)


class RepoResolver(object):

    def __init__(self, config, game_id):
        self.repositories = {}
        for repo_id, repo_cfg in Repositories.iteritems():
            if repo_id in config:
                self.repositories[repo_id] = NexusModsSession(repo_id, game_id, config[repo_id]['username'], config[repo_id]['password'])
            else:
                log.warn('Logging into %s with NULL credentials (Anonymous)', repo_id)
                self.repositories[repo_id] = NexusModsSession(repo_id, game_id, None, None)
        self.repositories['direct'] = DirectSession('direct', game_id)

    def _resolve_file_id(self, file_id):
        repo_id, game_id, file_id = file_id.split('/')
        return repo_id, game_id, file_id

    def _resolve_mod_id(self, mod_id):
        repo_id, game_id, mod_id = mod_id.split('/')
        return repo_id, game_id, int(mod_id)

    def ResolveFileKey(self, key):
        if key.startswith('http'):
            return self.repositories['direct'].ResolveFileKey(key)
        return key

    def GetFileDownload(self, file_id):
        '''
        @param file_id repo/file_id format.
        @returns URL to download the file from.
        '''
        repo_id, _, file_id = self._resolve_file_id(file_id)
        return self.repositories[repo_id].GetFileDownload(file_id)

    def GetModInfo(self, mod_id):
        '''
        @param mod_id repo/mod_id format.
        @returns ModInfo dict.
        '''
        repo_id, _, mod_id = self._resolve_mod_id(mod_id)
        return self.repositories[repo_id].GetModInfo(mod_id)

    def GetModFiles(self, mod_id, pack_config):
        '''
        @param mod_id repo/mod_id format.
        @returns List of FileInfo dicts.
        '''
        repo_id, _, mod_id = self._resolve_mod_id(mod_id)
        return self.repositories[repo_id].GetModFiles(mod_id, pack_config)

    def DownloadTo(self, file_id, to_path, dl_cfg):
        '''
        @param file_id repo/file_id format.
        @param to_path Where to put the downloaded file.
        @returns URL to download the file from.
        '''
        repo_id, _, file_id = self._resolve_file_id(file_id)
        return self.repositories[repo_id].DownloadTo(file_id, to_path, dl_cfg)
