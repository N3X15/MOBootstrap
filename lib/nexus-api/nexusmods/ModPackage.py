'''
BLURB GOES HERE.

Copyright (c) 2015 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''
import collections
import sys
import os
import hashlib
from buildtools import log, os_utils, utils
from _globals import MOB_VERSION


def CheckLock(filename, data):
    filedir = os.path.dirname(filename)
    os_utils.ensureDirExists(filedir)
    hashsum = hashlib.sha256(data).hexdigest()
    storedsum = ''
    result = False
    if os.path.isfile(filename):
        with open(filename, 'r') as f:
            storedsum = f.read().strip()
            if hashsum == storedsum:
                result = True
            else:
                with log.info('HASH MISMATCH:'):
                    log.info('Stored: {}'.format(storedsum))
                    log.info('Fresh : {}'.format(hashsum))
    return result


def SetLock(filename, data):
    with open(filename, 'w') as f:
        f.write(hashlib.sha256(data).hexdigest())


class ModPackage(object):

    def __init__(self):
        self.id = ''
        self.mod_id = 0
        self.repo_id = ''
        self.game_id = ''
        self.name = ''
        self.author = ''
        self.version = ''
        self.adult = False
        self.priority = 0

        self.requires = []
        self.conflicts = []

        self.files = {}
        self.optional = {}
        self.overrides = {}
        self.categories = []

        self.extract_from = '.'
        self.extract_to = 'DATA'

        self.hash = ''

    def GetUniqueID(self):
        return '{}/{}/{}'.format(self.repo_id, self.game_id, self.mod_id)

    def SetUniqueID(self, uid):
        self.repo_id, self.game_id, self.mod_id = uid.split('/')
        self.mod_id = int(self.mod_id)
        assert self.repo_id != ''
        assert self.game_id != ''
        assert self.mod_id != 0

    def CheckLock(self, cachedir):
        lockfile = os.path.join(cachedir, 'locks', self.repo_id, self.game_id, self.mod_id, 'mod.lock')
        return CheckLock(lockfile, self.hash + MOB_VERSION)

    def SetLock(self, cachedir):
        lockfile = os.path.join(cachedir, 'locks', self.repo_id, self.game_id, self.mod_id, 'mod.lock')
        SetLock(lockfile, self.hash + MOB_VERSION)

    def GetPakfile(self, root):
        return os.path.join(root, 'packages', self.repo_id, self.game_id, str(self.mod_id) + '.pak')

    # Happens BEFORE dependency checking.
    def canInstall(self, cfg, installed=None):
        for required in self.requires:
            if not self.checkRequirement(cfg, required, installed):
                return False

        for conflict in self.conflicts:
            if self.checkRequirement(cfg, conflict, installed):
                return False
        return True

    def checkRequirement(self, cfg, required, installed):
        if required.startswith(':'):
            directive, param = required.split(':')
            if directive == 'gamefile':
                install_dir = cfg.cfg['paths'][self.game_id]['install']
                if not os.path.isfile(os.path.join(install_dir, param)):
                    return False
            elif directive == 'package':
                if installed is not None and param not in installed:
                    return False
        return True

    def toObject(self):
        o = collections.OrderedDict([
            ('id', self.id),
            ('repo', self.repo_id),
            ('game_id', self.game_id),
            ('mod_id', self.mod_id),
            ('name', self.name),
            ('author', self.author),
            ('version', self.version),
            ('priority', self.priority)
        ])
        if self.adult:
            o['adult'] = self.adult
        if len(self.requires) > 0:
            o['requires'] = self.requires
        if len(self.files) > 0:
            o['files'] = [f.toObject() for f in self.files.values()]
        if len(self.optional) > 0:
            o['optional'] = [f.toObject() for f in self.optional.values()]
        if len(self.categories) > 0:
            o['categories'] = self.categories
        if len(self.overrides) > 0:
            o['overrides'] = self.overrides
        return o

    def save(self, root):
        import toml
        with open(self.GetPakfile(root), 'w') as f:
            data = toml.dumps(self.toObject())
            data = data.replace('[[', '\n[[')
            f.write(data)
            #yaml.dump(self.toObject(), f, default_flow_style=False)

    def load(self, root):
        import toml
        pf = self.GetPakfile(root)
        self.hash = utils.sha256sum(pf)
        with open(pf, 'r') as f:
            data = toml.loads(f.read())
            try:
                self.fromObject(data)
            except KeyError as e:
                self.__init__()
                log.error('Failed to load %s:', pf)
                log.error(e)
                sys.exit(1)

    def fromIndex(self, cfg):
        '''
        index.yml shit
        '''

        if 'name' in cfg:
            self.name = cfg['name']
        self.priority = cfg.get('priority', 0)
        self.requires = cfg.get('requires', [])
        self.conflicts = cfg.get('conflicts', [])

        if 'files' in cfg:
            for file_id, file_cfg in cfg['files'].iteritems():
                found = False
                for modfile in self.files.values() + self.optional.values():
                    if modfile.id == file_id or modfile.url == file_id:
                        modfile.fromIndex(file_cfg)
                        found = True
                if not found:
                    log.warn('File ID "%s" does not exist.  Check index.yml.', file_id)
                    # sys.exit(1)
        if 'overrides' in cfg:
            for modfile in self.files.values() + self.optional.values():
                modfile.fromIndex(cfg['overrides'])

    def fromObject(self, data):
        self.id = data['id']
        self.mod_id = data['mod_id']
        self.game_id = data['game_id']
        self.repo_id = data['repo']
        self.name = data['name']
        self.author = data['author']
        self.version = data['version']
        self.priority = data.get('priority', 0)
        self.adult = 'adult' in data

        self.requires = data.get('requires', [])
        self.conflicts = data.get('conflicts', [])
        self.categories = data.get('categories', [])
        self.extract_config = data.get('extraction', {})
        self.overrides = data.get('overrides', {})

        self.files = {}
        if 'files' in data:
            for fileinfo in data['files']:
                mf = ModFile()
                mf.fromObject(fileinfo)
                mf.mod_id = self.mod_id
                mf.mod_hash = self.hash
                mf.mod = self
                self.files[mf.file_id] = mf
        self.optional = {}
        if 'optional' in data:
            for fileinfo in data['optional']:
                mf = ModFile()
                mf.fromObject(fileinfo)
                mf.mod_id = self.mod_id
                mf.mod_hash = self.hash
                mf.mod = self
                self.optional[mf.file_id] = mf

    def __lt__(self, other):
        return self.priority < other.priority or self.name < other.name

    def __gt__(self, other):
        return self.priority > other.priority or self.name > other.name

    def __eq__(self, other):
        return self.priority == other.priority or self.name == other.name

    def __le__(self, other):
        return self.priority <= other.priority or self.name <= other.name

    def __ge__(self, other):
        return self.priority >= other.priority or self.name >= other.name

    def __ne__(self, other):
        return self.priority != other.priority or self.name != other.name


class FileCategory(object):
    MAIN = 0
    OPTIONAL = 1
    OLD = 2


class DependReport(object):
    MET = 0
    WAIT = 1
    SKIP = 2


class ModFile(object):

    def __init__(self):
        self.id = ''
        self.name = ''
        self.file_id = 0
        self.game_id = ''
        self.repo_id = ''
        self.requires = []
        self.conflicts = []
        self.skip = False
        self.optional = False
        self.mod = None  # Relationship tracking

        # MOB stuff
        # "Top level" of the mod, as far as MO's concerned.
        self.extract_from = '.'
        # Subdirectory of the game to extract to.
        self.extract_to = 'DATA'

        self.installer = None
        self.extract_config = {}
        self.download_config = {}
        self.category_id = 0
        self.priority = 0

        # For overriding download URLs.
        self.url = ''

    def CheckLock(self, cachedir):
        lockfile = os.path.join(cachedir, self.repo_id, self.game_id, str(self.mod_id), str(self.file_id) + '.lock')
        return CheckLock(lockfile, self.mod_hash + MOB_VERSION)

    def SetLock(self, cachedir):
        lockfile = os.path.join(cachedir, self.repo_id, self.game_id, str(self.mod_id), str(self.file_id) + '.lock')
        SetLock(lockfile, self.mod_hash + MOB_VERSION)

    def GetUniqueID(self, for_dl=False):
        if for_dl and self.url != '':
            return self.url
        return '{}/{}/{}'.format(self.repo_id, self.game_id, self.file_id)

    def SetUniqueID(self, uid):
        self.repo_id, self.game_id, self.file_id = uid.split('/')
        self.file_id = int(self.file_id)

    def fromIndex(self, cfg):
        '''
        index.yml shit
        '''
        if 'skip' in cfg:
            self.skip = cfg['skip']
        self.optional = 'optional' in cfg and cfg['optional'] == True

        self.name = cfg.get('name', self.name)
        self.id = cfg.get('id', self.id)

        self.requires = cfg.get('requires', [])
        self.conflicts = cfg.get('conflicts', [])

        self.extract_from = cfg.get('extract_from', '.')
        self.extract_to = cfg.get('extract_to', 'DATA')

        self.installer = None if 'installer' not in cfg else self.loadInstaller(cfg['installer'])

        self.extract_config = cfg.get('extraction', {})
        self.download_config = cfg.get('download', {})
        self.url = cfg.get('url', self.url)
        self.priority = cfg.get('priority', 0)

    def loadInstaller(self, cfg):
        if cfg['type'] == 'manual':
            from nexusmods.installer.manual import Manual
            I = Manual()
            I.Configure(cfg)
            return I
        if cfg['type'] == 'fomod':
            from nexusmods.installer.fomod import FOMod
            I = FOMod()
            I.Configure(cfg)
            return I
        else:
            log.warn('Unknown installer: %s', cfg['type'])
            return None

    def dependenciesMet(self, cfg, installed, lastchance):
        if self.skip:
            return DependReport.SKIP
        #if lastchance:
        #    log.info('Checking %s - last chance',self.GetUniqueID())
        for required in self.requires:
            report = self.checkRequirement(cfg, required, installed)
            if self.optional and report == DependReport.WAIT and lastchance:
                return DependReport.SKIP
            if report != DependReport.MET:
                return report

        for conflict in self.conflicts:
            report = self.checkRequirement(cfg, conflict, installed)
            if report == DependReport.MET:
                return DependReport.SKIP

        return DependReport.MET

    def canInstall(self, cfg):
        return self.dependenciesMet(cfg, None, False)

    def checkRequirement(self, cfg, required, installed):
        if ':' in required:
            directive, param = required.split(':')
            if directive == 'gamefile':
                install_dir = cfg.cfg['paths'][self.game_id]['install']
                if not os.path.isfile(os.path.join(install_dir, param)):
                    #log.info('Need %s', required)
                    return DependReport.SKIP
            elif directive in ('modfile', 'package') and installed is not None:
                if required not in installed:
                    #log.info('Need %s', required)
                    return DependReport.WAIT
        return DependReport.MET

    def toObject(self):
        o = collections.OrderedDict([
            ('id', self.id),
            ('name', self.name),
            ('repo_id', self.repo_id),
            ('game_id', self.game_id),
            ('file_id', self.file_id),
        ])
        if self.extract_from != '.':
            o['extract_from'] = self.extract_from
        if self.extract_to != 'DATA':
            o['extract_to'] = self.extract_to
        if self.url != '':
            o['url'] = self.url
        if self.skip:
            o['skip'] = self.skip
        if self.optional:
            o['optional'] = True
        if self.priority != 0:
            o['priority'] = self.priority
        if len(self.requires) > 0:
            o['requires'] = self.requires
        if len(self.conflicts) > 0:
            o['conflicts'] = self.conflicts
        if self.installer is not None:
            o['installer'] = self.installer.getConfig()
        if len(self.extract_config):
            o['extraction'] = self.extract_config
        if len(self.download_config):
            o['download'] = self.download_config

        return o

    def fromObject(self, data):
        self.id = data['id']
        self.name = data['name']
        self.repo_id = data['repo_id']
        self.game_id = data['game_id']
        self.file_id = data['file_id']

        self.skip = 'skip' in data
        self.optional = 'optional' in data

        self.extract_config = data.get('extraction', {})

        self.requires = data.get('requires', [])
        self.conflicts = data.get('conflicts', [])

        self.extract_from = data.get('extract_from', '.')
        self.extract_to = data.get('extract_to', 'DATA')

        self.installer = None if 'installer' not in data else self.loadInstaller(data['installer'])
        self.extract_config = data.get('extraction', {})
        self.download_config = data.get('download', {})
        self.url = data.get('url', '')
        self.priority = data.get('priority', 0)

    def writeMetaINI(self, mod, metafile, dlfile):
        with open(metafile, 'w') as f:
            f.write('[General]\n')
            f.write('modid={}\n'.format(self.mod.mod_id))
            f.write('version={}\n'.format(self.mod.version))
            f.write('newestVersion={}\n'.format(self.mod.version))
            f.write('category={}\n'.format(','.join([str(x) for x in self.mod.categories])))
            f.write('installationFile={}\n'.format(dlfile))
            f.write('repository={}\n'.format('Nexus' if self.repo_id == 'nexus' else self.repo_id))
            f.write('ignoredVersion=\n')
            f.write('notes=Installed by MOBootstrap v{}\n'.format(MOB_VERSION))
            f.write('nexusDescription=""\n')
            f.write('lastNexusQuery=\n')
            f.write('endorsed=0\n')
            f.write('\n')
            f.write('[installedFiles]\n')
            f.write('1\\modid={}\n'.format(mod.mod_id))
            f.write('1\\fileid={}\n'.format(self.file_id))
            f.write('size=1\n')

    def writeDLMeta(self, mod, metafile, dlfile):
        with open(metafile, 'w') as f:
            f.write('[General]\n')
            f.write('modID={}\n'.format(mod.mod_id))
            f.write('fileID={}\n'.format(self.file_id))
            f.write('url="{}"\n'.format(dlfile))
            f.write('name={}\n'.format(mod.name))
            f.write('version={}\n'.format(mod.version))
            f.write('newestVersion={}\n'.format(mod.version))
            f.write('repository={}\n'.format('Nexus' if self.repo_id == 'nexus' else ''))
            f.write('installed=true\n')
            f.write('uninstalled=false\n')
            f.write('paused=false\n')
            f.write('removed=false\n')

    def __lt__(self, other):
        return self.priority < other.priority or self.name < other.name

    def __gt__(self, other):
        return self.priority > other.priority or self.name > other.name

    def __eq__(self, other):
        return self.priority == other.priority or self.name == other.name

    def __le__(self, other):
        return self.priority <= other.priority or self.name <= other.name

    def __ge__(self, other):
        return self.priority >= other.priority or self.name >= other.name

    def __ne__(self, other):
        return self.priority != other.priority or self.name != other.name
