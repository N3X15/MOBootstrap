'''
VDF Serialization

Copyright (c) 2015 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''
import collections, logging
import pyparsing as pyp

# Stolen from Watchdog, my other MIT-Licensed project.

def _toDict(s,l,t): #IGNORE:unused-argument
    out = {}
    #print(t[0])
    for k,v in t:
        out[k]=v
    return out

class VDFNode:
    def __init__(self, root=False):
        self.children = collections.OrderedDict()
        self.is_root = root

    @classmethod
    def FromDict(cls, _dict, root=True):
        node = VDFNode(root=root)
        for k, v in _dict.items():
            if isinstance(v, (dict,collections.OrderedDict)):
                v = VDFNode.FromDict(v, False)
            node.children[k] = v
        return node

    @classmethod
    def Parse(cls,string,errmethod=logging.error):
        Comment = pyp.dblSlashComment
        OpenBracket = pyp.Suppress('{')
        CloseBracket = pyp.Suppress('}')
        Map = pyp.Forward()
        Value = pyp.QuotedString('"',escChar='\\')
        ValuePair = pyp.Group((Value + Value) | (Value + Map))
        MapContents = pyp.ZeroOrMore(ValuePair).setParseAction(_toDict)
        Map << OpenBracket + MapContents + CloseBracket
        VDFSyntax = pyp.OneOrMore(ValuePair).setParseAction(_toDict)
        VDFSyntax.ignore(Comment)

        try:
            res = VDFSyntax.parseString(string)
        except pyp.ParseException, err:
            errmethod(err.line)
            errmethod("-"*(err.column-1) + "^")
            errmethod(err)
            return None
        #pprint.pprint(res)
        return res.asList()[0]

    def serialize(self, level=0):
        o = ''
        outer_level = level
        inner_level = level + 1
        if not self.is_root:
            o += '{\n'
        else:
            inner_level = 0
            outer_level = 0
        outer_indent = '\t' * outer_level
        inner_indent = '\t' * inner_level
        padlen = 0
        for key in self.children.keys():
            keylen = len(key)
            if padlen < keylen:
                padlen = keylen
        for k, v in self.children.items():
            padding = ' ' * (padlen - len(k) + 1)
            o += inner_indent
            o += '"{}"'.format(k)
            o += padding
            if isinstance(v, VDFNode):
                o += v.serialize(inner_level)
            else:
                o += '"{}"\n'.format(v)
        if not self.is_root:
            o += outer_indent + '}\n'
        return o

    def __str__(self):
        return self.serialize()

    def toDict(self):
        return dict(self.children)

class VDFFile:
    def __init__(self, value=None):
        if value is None:
            self.rootnode = VDFNode()
        else:
            self.rootnode = VDFNode.FromDict(value)

    def toDict(self):
        return self.rootnode.toDict()

    @staticmethod
    def LoadFile(filename):
        vdf = VDFFile()
        vdf.load(filename)
        return vdf

    def save(self, filename):
        with open(filename, 'w') as f:
            f.write(self.rootnode.serialize())

    def load(self, filename):
        with open(filename, 'r') as f:
            self.rootnode.children = VDFNode.Parse(f.read())
