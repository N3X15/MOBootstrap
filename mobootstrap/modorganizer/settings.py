'''
QSettings-based serialization

Copyright (c) 2015 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''
import collections
import os

from buildtools.qconfig import QConfig

from buildtools.bt_logging import log
from buildtools.config import ConfigFile


class MOSettings(QConfig):

    '''
    A wrapper around QConfig, which is a wrapper around QSettings.
    '''

    def __init__(self, filename, default={}, template_dir='.', variables={}):
        super(MOSettings, self).__init__(filename, default, template_dir, variables)

        self.base_apps = ('SKSE', 'Skyrim', 'Skyrim Launcher', 'Creation Kit')
        self.base_app_defaults = collections.OrderedDict({
            'custom': True,
            'ownicon': False,
            'toolbar': False
        })
        self.custom_app_defaults = collections.OrderedDict({
            'arguments': '',
            'workingDirectory': '',
            'closeOnStart': False,
            'steamAppID': '',
        })

    def applications(self):
        apps = {}
        for k, app in self.get('customExecutables', collections.OrderedDict()).iteritems():
            if k == 'size':
                continue
            apps[app['title']] = app
        return apps

    def setApplication(self, name, **kwargs):
        if name not in self.applications():
            return self.addApplication(**kwargs)
        noisy = False
        if 'noisy' in kwargs:
            noisy = True
            del kwargs['noisy']
        applist = self.get('customExecutables', collections.OrderedDict())
        nitems = 0
        for k, app in applist.iteritems():
            if k == 'size':
                continue
            if app['title'] == name:
                if app['title'] in self.base_apps:
                    applist[k] = dict(self.base_app_defaults)
                else:
                    applist[k] = dict(self.custom_app_defaults)
                for key, value in kwargs.iteritems():
                    applist[k][key] = value
                #log.info('Updated customExecutables/%d (%s)',k,name)
            nitems += 1
        if 'customExecutables' in applist:
            del applist['customExecutables']
        applist['size'] = str(nitems)
        self.cfg['customExecutables'] = applist

    # 9\custom=true
    # 9\toolbar=false
    # 9\ownicon=false
    # 9\binary=C:/Windows/System32/cmd.exe
    # 9\arguments=/C \"C:\\Program Files (x86)\\Mod Organizer\\mods\\Real Shelter - Full Release\\R.S.Patcher\\RSPatcher.bat\"
    # 9\workingDirectory=F:/ModOrganizer/mods/Real Shelter - Full Release/R.S.Patcher
    # 9\closeOnStart=false
    # 9\steamAppID=
    def addApplication(self, **app):
        applist = self.get('customExecutables', collections.OrderedDict())
        noisy = False
        if 'noisy' in app:
            noisy = True
            del app['noisy']
        if app['title'] in applist:
            if noisy:
                log.info('%s already present, skipping.', app['title'])
            return
        if noisy:
            log.info('Adding %s (%s)...', app['title'], app['binary'])
        if 'size' in applist:
            del applist['size']
        new_app = None
        if app['title'] in self.base_apps:
            new_app = dict(self.base_app_defaults)
        else:
            new_app = dict(self.custom_app_defaults)
        for key, value in app.iteritems():
            new_app[key] = value
        applist[str(len(applist) + 1)] = new_app
        applist['size'] = str(len(applist))
        if 'customExecutables' in applist:
            del applist['customExecutables']
        self.cfg['customExecutables'] = applist
