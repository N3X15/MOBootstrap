'''
BLURB GOES HERE.

Copyright (c) 2015 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''
import yaml
import sys
import os
from dlgPathSelect import Ui_dlgPathSelect
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import Qt
from mobootstrap.utils import GetIcon
from buildtools.bt_logging import log


class dlgPathSelectImpl(QtGui.QDialog, Ui_dlgPathSelect):

    def __init__(self, config, bundles, parent=None, f=QtCore.Qt.WindowFlags()):
        QtGui.QDialog.__init__(self, parent, f)
        self.setupUi(self)
        # self.setWindowIcon(GetIcon('mobootstrap.png'))
        self.setFixedSize(self.size())
        self.setModal(True)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.config = config
        self.bundles = bundles

        defaultBasePath = os.path.join('C:\\', 'SkyrimTools')
        skyrimpath = self.config.get('paths.skyrim.install', '')
        if not os.path.isdir(skyrimpath):
            skyrimpath = self.findSkyrim()
        self.txtSkyrimInstall.setText(skyrimpath)
        self.txtModOrganizer.setText(self.config.get('paths.mod-organizer', os.path.join(defaultBasePath, 'ModOrganizer')))
        self.txtENBChanger.setText(self.config.get('paths.enbchanger', os.path.join(defaultBasePath, 'ENBChanger')))
        self.txtSGSC.setText(self.config.get('paths.sgsc', os.path.join(defaultBasePath, 'SaveGameScriptCleaner')))
        self.txtLOOT.setText(self.config.get('paths.loot', os.path.join(defaultBasePath, 'LOOT')))

    def findSkyrim(self):
        with log.info('Searching for Skyrim install directory...'):
            from mobootstrap.steam.vdf import VDFFile
            from valve.steam.client import SteamClient
            steamcli = SteamClient()
            # C:\\Program Files (x86)\\Steam\\SteamApps\\libraryfolders.vdf
            library_folders = [
                'C:\\Program Files (x86)\\Steam'
            ]
            libfoldervdf=os.path.abspath(os.path.join(steamcli.path, 'steamapps', 'libraryfolders.vdf'))
            if not os.path.isfile(libfoldervdf):
                log.warn('Unable to locate libraryfolders.vdf (Expected: %s)',libfoldervdf)
            else:
                log.info('Reading %s...',libfoldervdf)
                vdfdata={}
                vdfdata = VDFFile.LoadFile(libfoldervdf).toDict()
                for key, value in vdfdata['LibraryFolders'].iteritems():
                    if key not in ("TimeNextStatsReport", "ContentStatsID"):
                        library_folders.append(value)

            for searchpath in library_folders:
                checkpath = os.path.join(searchpath, 'steamapps', 'common', 'Skyrim')
                if os.path.isdir(checkpath):
                    log.info('FOUND at ' + checkpath)
                    return checkpath
            return ''

    @QtCore.pyqtSlot()  # signal with no arguments
    def accept(self):
        self.config.set('paths.skyrim.install', unicode(self.txtSkyrimInstall.text()))
        self.config.set('paths.mod-organizer', unicode(self.txtModOrganizer.text()))
        self.config.set('paths.enbchanger', unicode(self.txtENBChanger.text()))
        self.config.set('paths.sgsc', unicode(self.txtSGSC.text()))
        self.config.set('paths.loot', unicode(self.txtLOOT.text()))
        # self.success_callback()
        super(dlgPathSelectImpl, self).accept()

    @QtCore.pyqtSlot()  # signal with no arguments
    def reject(self):
        sys.exit(1)
