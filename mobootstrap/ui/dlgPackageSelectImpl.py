'''
BLURB GOES HERE.

Copyright (c) 2015 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''
import yaml
import sys
from dlgPackageSelect import Ui_dlgPackageSelect
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import Qt
from mobootstrap.utils import GetIcon


class dlgPackageSelectImpl(QtGui.QDialog, Ui_dlgPackageSelect):

    def __init__(self, config, bundles, success_callback, parent=None, f=QtCore.Qt.WindowFlags()):
        QtGui.QDialog.__init__(self, parent, f)
        self.setupUi(self)
        # self.setWindowIcon(GetIcon('mobootstrap.png'))
        self.setFixedSize(self.size())
        self.setModal(True)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.model = QtGui.QStandardItemModel(self.lstPackages)
        self.bundles = {}
        self.bundle_data = bundles
        self.config = config
        self.success_callback = success_callback
        for bundle_id in self.bundle_data.cfg.keys():
            self.bundles[bundle_id] = self.bundle_data[bundle_id].get('default', bundle_id in config.get('bundles', []) or self.bundle_data[bundle_id]['settings'].get('default', False))
            item = QtGui.QStandardItem(bundle_id)
            item.setCheckable(True)
            item.setCheckState(Qt.Checked if self.bundles[bundle_id] else Qt.Unchecked)
            self.model.appendRow(item)
        self.lstPackages.setModel(self.model)

        self.model.itemChanged.connect(self.on_item_changed)

        self.txtNexusUsername.setText(self.config.get('auth.nexus.username', ''))
        self.txtNexusPassword.setEchoMode(QtGui.QLineEdit.Password)
        self.txtNexusPassword.setText(self.config.get('auth.nexus.password', ''))

    @QtCore.pyqtSlot(QtCore.QObject)  # signal with no arguments
    def on_item_changed(self, item):
        bundle_id = unicode(item.text())
        self.bundles[bundle_id] = item.checkState()
        print("{} has been {}".format(bundle_id, 'checked' if item.checkState() else 'unchecked'))

    @QtCore.pyqtSlot()  # signal with no arguments
    def accept(self):
        self.config.set('auth.nexus.username', unicode(self.txtNexusUsername.text()))
        self.config.set('auth.nexus.password', unicode(self.txtNexusPassword.text()))
        self.config.set('bundles', [x[0] for x in self.bundles.iteritems() if x[1]])
        # self.success_callback()
        super(dlgPackageSelectImpl, self).accept()

    @QtCore.pyqtSlot()  # signal with no arguments
    def reject(self):
        sys.exit(1)
