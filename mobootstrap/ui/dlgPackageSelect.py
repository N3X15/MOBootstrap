# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mobootstrap/ui\dlgPackageSelect.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_dlgPackageSelect(object):
    def setupUi(self, dlgPackageSelect):
        dlgPackageSelect.setObjectName(_fromUtf8("dlgPackageSelect"))
        dlgPackageSelect.resize(455, 372)
        dlgPackageSelect.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.gridLayout = QtGui.QGridLayout(dlgPackageSelect)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.buttonBox = QtGui.QDialogButtonBox(dlgPackageSelect)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.gridLayout.addWidget(self.buttonBox, 1, 1, 1, 1)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.grpLoginDetails = QtGui.QGroupBox(dlgPackageSelect)
        self.grpLoginDetails.setObjectName(_fromUtf8("grpLoginDetails"))
        self.gridLayout_3 = QtGui.QGridLayout(self.grpLoginDetails)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.formLayout = QtGui.QFormLayout()
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.lblNexusUsername = QtGui.QLabel(self.grpLoginDetails)
        self.lblNexusUsername.setObjectName(_fromUtf8("lblNexusUsername"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.lblNexusUsername)
        self.txtNexusUsername = QtGui.QLineEdit(self.grpLoginDetails)
        self.txtNexusUsername.setObjectName(_fromUtf8("txtNexusUsername"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.txtNexusUsername)
        self.lblNexusPassword = QtGui.QLabel(self.grpLoginDetails)
        self.lblNexusPassword.setObjectName(_fromUtf8("lblNexusPassword"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.lblNexusPassword)
        self.txtNexusPassword = QtGui.QLineEdit(self.grpLoginDetails)
        self.txtNexusPassword.setObjectName(_fromUtf8("txtNexusPassword"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.txtNexusPassword)
        self.gridLayout_3.addLayout(self.formLayout, 0, 0, 1, 1)
        self.verticalLayout.addWidget(self.grpLoginDetails)
        self.grpPackageList = QtGui.QGroupBox(dlgPackageSelect)
        self.grpPackageList.setObjectName(_fromUtf8("grpPackageList"))
        self.gridLayout_2 = QtGui.QGridLayout(self.grpPackageList)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.lstPackages = QtGui.QListView(self.grpPackageList)
        self.lstPackages.setFrameShape(QtGui.QFrame.StyledPanel)
        self.lstPackages.setProperty("showDropIndicator", False)
        self.lstPackages.setAlternatingRowColors(True)
        self.lstPackages.setViewMode(QtGui.QListView.ListMode)
        self.lstPackages.setObjectName(_fromUtf8("lstPackages"))
        self.gridLayout_2.addWidget(self.lstPackages, 0, 0, 1, 1)
        self.verticalLayout.addWidget(self.grpPackageList)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 2)

        self.retranslateUi(dlgPackageSelect)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), dlgPackageSelect.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), dlgPackageSelect.reject)
        QtCore.QMetaObject.connectSlotsByName(dlgPackageSelect)
        dlgPackageSelect.setTabOrder(self.txtNexusUsername, self.txtNexusPassword)
        dlgPackageSelect.setTabOrder(self.txtNexusPassword, self.lstPackages)
        dlgPackageSelect.setTabOrder(self.lstPackages, self.buttonBox)

    def retranslateUi(self, dlgPackageSelect):
        dlgPackageSelect.setWindowTitle(_translate("dlgPackageSelect", "MOBootstrap - Package Selection", None))
        self.grpLoginDetails.setTitle(_translate("dlgPackageSelect", "NexusMods Credentials", None))
        self.lblNexusUsername.setText(_translate("dlgPackageSelect", "Username:", None))
        self.lblNexusPassword.setText(_translate("dlgPackageSelect", "Password:", None))
        self.grpPackageList.setTitle(_translate("dlgPackageSelect", "Packages", None))


class dlgPackageSelect(QtGui.QDialog, Ui_dlgPackageSelect):
    def __init__(self, parent=None, f=QtCore.Qt.WindowFlags()):
        QtGui.QDialog.__init__(self, parent, f)

        self.setupUi(self)


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    dlgPackageSelect = QtGui.QDialog()
    ui = Ui_dlgPackageSelect()
    ui.setupUi(dlgPackageSelect)
    dlgPackageSelect.show()
    sys.exit(app.exec_())

