'''
prepare-repo.py - Configures repository environment for compiling.

Copyright (c) 2015 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''
import os
import subprocess
import sys
import platform
import pip

PLATFORM = platform.system()
PYTHON2_DIR = "C:\\Python27\\"
UIC = os.path.join(PYTHON2_DIR, 'Lib', 'site-packages', 'PyQt4', 'pyuic4.bat')
PYINSTALLER_DIR = 'lib/pyinstaller-2.0/'
PYINSTALLER_FLAGS = ['-w']
UIC_FLAGS = ['--pyqt3-wrapper']
UI_DIR = 'mobootstrap/ui'


def _cmd(command, show_output=False):
    #new_env = _cmd_handle_env(env)
    #command = _cmd_handle_args(command)
    print('$ ' + (' '.join(command)))
    if show_output:
        return subprocess.call(command, shell=False) == 0
    else:
        return subprocess.check_output(command, stderr=subprocess.STDOUT)


def _which(program):
    fpath, _ = os.path.split(program)
    if fpath:
        if _is_executable(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if _is_executable(exe_file):
                return exe_file
    return None


def _is_executable(fpath):
    if sys.platform == 'win32':
        if not fpath.endswith('.exe'):
            fpath += '.exe'
    return os.path.isfile(fpath) and os.access(fpath, os.X_OK)


def _assertWhich(program):
    fullpath = _which(program)
    if fullpath is None:
        errmsg = '{executable} is not in PATH!'.format(executable=program)
        raise RuntimeError(errmsg)
    else:
        print(' * Found: {}'.format(fullpath))
    return fullpath

print('Environment Info:')
print(' sys.platform = ' + sys.platform)
print(' platform.system() = ' + PLATFORM)
print('Performing pre-flight checks...')
_assertWhich('git')
_assertWhich('python')
_assertWhich('pip')
print('Completed pre-flight checks, installing PBT...')

script_dir = os.path.dirname(os.path.realpath(__file__))

PBT_PATH = os.path.join(script_dir, 'lib', 'python-build-tools')
if not os.path.isdir(PBT_PATH):
    os.makedirs(PBT_PATH)
_cmd(['git', 'submodule', 'update', '--init', '--recursive', '--remote'], show_output=True)

os.chdir('lib/python-build-tools')
_cmd(['python', 'setup.py', 'develop'], show_output=True)
os.chdir(script_dir)
print('Importing PBT...')

sys.path.append(os.path.join(script_dir, 'lib', 'python-build-tools'))

from buildtools import *

log.info('PBT successfully booted.')

with log.info('Checking PyQt4...'):
    import PyQt4
    log.info('Seems to be okay.')

with log.info('Installing pip packages...'):
    cmd(['pip', 'install', '-I', 'toml', 'pyyaml', 'Jinja2', 'requests', 'lxml', 'pyparsing', 'pyinstaller'], show_output=True, echo=True)

with log.info('Building UI...'):
    UI_FILES = [os.path.join(UI_DIR, f) for f in os.listdir(UI_DIR) if f.endswith('.ui')]
    for uifile in UI_FILES:
        outfile = uifile.replace('.ui', '.py')
        with log.info('Building %s...', outfile):
            cmd([UIC] + UIC_FLAGS + ['-x', uifile, '-o', outfile], critical=True)

    UI_PY_FILES = [os.path.join(UI_DIR, f) for f in os.listdir(UI_DIR) if f.endswith('.py')]
log.info('Ready to run!  type "compile.bat" or run "python bootstrap.py".')
